import datamunging.data_extractor as data_ext
from datamunging.data_analyzer import DataAnalyzer
import re

class Football:

    def __init__(self, data_file_path):
        self.extractor = data_ext.DataExtractor(data_file_path)
        self.analyzer = DataAnalyzer()

    def calculate_min_difference(self):
        data = self.extractor.extract_data()
        data_list = list()
        for index in range(0, len(data['Team'])):
            team, goal_f, goal_a = data['Team'].__getitem__(index),re.sub("[^0-9]","", data['F'].__getitem__(index)), data[
                'A'].__getitem__(index)
            data_list.append((team, goal_f, goal_a))

        return self.analyzer.calculate_min_difference(data_list)


if __name__ == "__main__":
    weather = Football('./data/football.dat')
    result = weather.calculate_min_difference()
    print(result)

