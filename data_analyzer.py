import re
class DataAnalyzer:
    def calculate_min_difference(self, data_list):
        min_difference=int()
        min_difference_key=str()
        min_difference_initilize_flag=False
        for data in data_list:

            difference =abs(int(re.sub("[^0-9]","",data[1])) - int(re.sub("[^0-9]","",data[2])))
            if min_difference_initilize_flag is False:
                min_difference_initilize_flag = True
                min_difference=difference

            if difference <= min_difference:
                min_difference_key=data[0]
                min_difference=difference
        min_value={min_difference_key:min_difference}
        return min_value

