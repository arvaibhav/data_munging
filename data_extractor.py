
class DataExtractor:

    def __init__(self, file_path):
        self.file_path = file_path

    def extract_data(self):
        data_dictionary= dict()
        with open(self.file_path, "r") as f:
            rows = f.readlines()
            header = rows[0].split()

            header_index_range = dict()
            for index in range(0, len(header)):
                current_header = header[index]
                headers_row = rows[0]
                start_index = headers_row.index(current_header)
                if index != len(header) - 1:
                    next_header = header[index + 1]
                    end_index = headers_row.index(next_header)
                    # start_index-1  to read from 1 index before
                    # end_index -2 to stop reading 1 index before next header
                    header_index_range[current_header] = {'start index': start_index - 1 if start_index > 1 else 0,
                                                          'end index': end_index - 2}
                else:
                    end_index = len(headers_row) - 1
                    header_index_range[current_header] = {'start index': start_index - 1 if start_index > 1 else 0,
                                                          'end index': end_index}


            for index in range(0, len(rows)):
                # remove empty rows and other unwanted rows
                row = rows[index]
                if row.strip() != '' and row.strip().count('-') != len(row.strip()):
                    for header_name, header_name_index in header_index_range.items():
                        if header_name in data_dictionary:
                            data_dictionary[header_name].append(
                                row[header_name_index['start index']:header_name_index['end index'] + 1].strip())
                        else:
                            data_dictionary[header_name] = list()

        return data_dictionary

